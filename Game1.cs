﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Game1
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        /// <summary>
        /// Zmienne Tworzone przy utworzeniu projektu.
        /// Pierwsza z nich służy do pobierania parametrów ekranu z naszego urządzenia mobilnego tzn szerokość, wysokość, koordy tapnięcia, rodzaj tapnięcia itd
        /// Druga z nich odpowiada za hm... generowanie i rysowanie po polu pracy, czyli po wyświetlaczu. Głownie wykorzystywana w metodzie Draw()
        /// </summary>
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        /// <summary>
        /// Zmienne związane z grą
        /// Jak na razie będziemy wykorzystywać kilka podstawiwych typów.
        /// Textura2D - jak sama nazwa wskazuje, będzie ona służyła do przechowywania spritów i innych obiektów graficznych,
        /// które będą zawarte w grze. To w niej można bezpośrednio manewrować grafiką tzn skalować, obracać, dodawać tło itd.
        /// Vector2 - klasa służąca do przechowywanie pozycji naszej tekstury. punkt (0,0) czyli początek osi znajduje się w lewym górnym rogu urzązenia (nie ważne
        /// w jaki sposób jest zorientowane). Prawy dolny róg to max szerokość i wysokość np: 800x600. Klasa Vector2 będzie zawierać referencję do odpowiedniej 
        /// tekstury i wyświetlać ją na ekranie tak jak sobie rzyczy tego programista
        /// </summary>
        private Texture2D tekstura;
        private Vector2 pozycja;
        private Vector2 pozycjaEkranu;
        private float rotacja;
        private float skala = 1f; //f - bo wartość należy do float


        /// <summary>
        /// Konstruktor. Jak na razie jest nie potrzebny - przynajmniej do prostych zastosowań. Wystarczy zostawić go tak jak jest, bo większość pracy
        /// będziemy robić w metodzie Update lub Initialize
        /// </summary>
        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// 
        /// To ta jakb metoda startowa dla gry. To tutaj ustawia sie wszystkie zmienne startowe, które mają byś wczytane przy starcie tzn sprawdza się czy jest
        /// internet jeśli jest wymagany, wczytuje SAVE'y, tworzy jakieś połączenia P2P itd
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// 
        /// Oddzielna metoda do wczytywania wszystkich plików zewnętrznych - muzyka, sprite, zdjęcia, animacje itd. To tutaj można ustawić jakieś zmienne
        /// "Globalne", które będą się odnosić do jakiegoś obiektu, czy danego urzązenia np punkty zaczepinia
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here

            tekstura = Content.Load<Texture2D>("Spacecraft");
            Viewport viewport = graphics.GraphicsDevice.Viewport;

            // punkt zaczepienia do tekstury
            pozycja.X = tekstura.Height / 2;
            pozycja.Y = tekstura.Width / 2;

            // punkt zaczepienia dla ekranu
            pozycjaEkranu.X = viewport.Width / 2;
            pozycjaEkranu.Y = viewport.Height / 2;
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// 
        /// A tego nie wiem gdzie i kiedy się używa - ale się dowiem :) Pewnie do zwalniania jakichś zasobów, ale z tego co czytam to metoda
        /// uruchamiana tylko raz w trakcie gry. Może na końcu?
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// 
        /// Metoda gry. Jest ona wywoływana w pętli 60 razy na sekundę i ona wywołuje metodę Draw, która rysuje nowe obiekty. W metodzie Update
        /// umieszczona jest cała logika związana z grą. Czyli przeliczane są obiekty, sprawdzane są intrakcje pomiędzy nimi i wszystko co wiąże się z ich "życiem"
        /// gameTime to zmienna pomocnicza na podstawie, której można np poruszać NPC czy robić jakieś obroty.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // To ponieżej sprawdza czy nie został wciśnięty przycisk back, jeśli tak - wyjdź z aplikacji - taki ficzer :P
            if(GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
            {
                this.Exit();
            }

            // TODO: Add your update logic here
            float czas = (float)gameTime.ElapsedGameTime.TotalSeconds;

            rotacja += czas;
            float kolo = MathHelper.Pi * 2; // dwójka jest potrzebna, aby był pełny obrót
            rotacja = rotacja % kolo;

            // Update powinien być zawsze na końcu bo to on wywołuje metode draw z nowymi parametrami
            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// 
        /// Metoda przerysowująca ekran gry, wywoływana przez Update(). Ogólnie wszystko co ma się pojawić na ekranie musi być pomiędzy begin() i end() zmiennej 
        /// spriteBatch. Ważna jest również kolejność rysowanie obiektów, bo jedne mogą przyzłaniać drugie
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);
            spriteBatch.Begin();

            // statyczny obiekt, narysowany w pozycji W200, H50 - Kolor white oznacza, że jak jest to transparent, to takowym pozostanie - zresztą, zawsze można potestować
            spriteBatch.Draw(tekstura, new Vector2(100, 50), Color.White);


            // obiekt kręci się w okolo puktu srodka ekranu "pozycja ekranu", ale niew iem dlaczego nie dziala tutaj zmienna "pozucja" - powinien on się krecic w okolo wlasnej osi
            spriteBatch.Draw(tekstura, pozycjaEkranu, null, Color.White, rotacja, pozycja, skala, SpriteEffects.None,0);

            // Obiekt kręci się w okolo wlasnej osi w punkcie (100,400), swiadczy o tym parametr new Vector2(tekstura.Width / 2, tekstura.Height / 2) - nie wiem dlaczego nie chce tutaj poprawnie dzialac smienna pozycja
            spriteBatch.Draw(tekstura, new Rectangle(100, 500, tekstura.Width / (int)skala, tekstura.Height / (int)skala), null, Color.White, rotacja, new Vector2(tekstura.Width / 2, tekstura.Height / 2), SpriteEffects.None, 0);


            // Obiekt kręci się w okolo punktu srodka w lasnej osi (100,500) w odleglosci 1,5 od niej(100,500), swiadczy o tym parametr new Vector2(tekstura.Width * (float)1.5, tekstura.Height / (float)1.5), ma rotację wsteczna
            // Tam się tworzy taki niewidzialny prostokat w okol ktorego wiruje obiekt wiec zmiana z 1.5 na np 2 zwiekszy promien, a nie spowoduje, ze obiekt bedzie wirowal po owalu
            spriteBatch.Draw(tekstura, new Rectangle(100, 500, tekstura.Width / (int)skala, tekstura.Height / (int)skala), null, Color.White, -rotacja, new Vector2(tekstura.Width * (float)1.5, tekstura.Height / (float)1.5), SpriteEffects.None, 0);

            spriteBatch.End();

            // TODO: Add your drawing code here

            base.Draw(gameTime);
        }
    }
}
